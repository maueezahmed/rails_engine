$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "community/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "community"
  s.version     = Community::VERSION
  s.authors     = ["Maueez Ahmed"]
  s.email       = ["maueezahmed@gmail.com"]
  s.homepage    = "https://bitbucket.org/maueezahmed/"
  s.summary     = "Discussion Forums"
  s.description = "Discussion Forums"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", "~> 5.1.4"

  s.add_development_dependency "sqlite3"
end
