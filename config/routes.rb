Community::Engine.routes.draw do
  resources :forums
  root to: "forums#index"
end
