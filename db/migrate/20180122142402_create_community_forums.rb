class CreateCommunityForums < ActiveRecord::Migration[5.1]
  def change
    create_table :community_forums do |t|
      t.title :name
      t.text :description

      t.timestamps
    end
  end
end
